import requests
from pprint import pprint
from config import load_config, load_thing, update_thing
from Auth import Authentication
import os

class Service:



    def __init__(self, thing_id=None):
        self.s = requests.Session()
        conf = load_config()
        self.base_url = conf['base_url']
        self.lang = conf['lang']
        self.userId = conf['user_id']
        
        self.s.headers.update({'access_token': conf['jwt']['token']})
        if thing_id:
            self.thing = self.getThing(thing_id)
        self.timeout_error_message = {
            'spa': 'Tiempo de espera agotado',
            'eng': 'Connection Timeout'
        }

        self.connection_error_message = {
            'spa': 'Error de Conexion',
            'eng': 'Conexion Error'
        }

        self.reconecting_message = {
            'eng': 'Trying to reconnect',
            'spa': 'Tratando de Reconectar'
        }

    def validateJwt(self):
        user_id = ''
        try:
            user_id = '/'+ self.userId
        except Exception:
            pass
        dummy = self.s.get('/user'+user_id)
        if dummy.status_code == 403:
            auth = Authentication()
            if auth.is_auth:
                return True
            else:
                return False
        elif dummy.status_code == 200:
            return True
        else:
            return False
    
    def send_measure(self, measure):
        try:
            self.s.post(
                self.base_url+'/measure',
                data=measure,
                timeout=10
            )
        except TimeoutError as timeout:
            print(self.timeout_error_message[self.lang])
        except Exception:
            print(self.connection_error_message[self.lang])
    
    def setThing(self, thing):
        id = thing['id']
        self.thing = self.getThing(id=id)

    def getThing(self, id=None):
        if not id:
            id = self.thing['id']
        try:
            res = self.s.get(self.base_url+'/thing/{}'.format(id))
            if(res.status_code == 200):
                return res.json()
            elif(res.status_code == 403):
                #JWT expired
                self.validateJwt()
            elif(res.status_code == 404):
                #Definetely deleted
                print('404')
                os._exit(1)
            else:
                #One posible thing happening here is the deleting of the Thing
                print('Error getting the thing')
                return None
        except Exception:
            print(self.connection_error_message[self.lang])

    def getUpdateRefreshRate(self):
        aux = self.getThing()
        print('polling')
        if aux and aux['refreshRate'] != self.thing['refreshRate']:
            print('refresh rate updated to {}'.format(aux['refreshRate']))
            self.thing['refreshRate'] = aux['refreshRate']
            return True
        return False

    
    def create_sensor(self, sensor):
        sensor['thing'] = self.thing['id']
        r = self.s.post(
            self.base_url+'/sensor',
            data=sensor)
        pprint(r.json())
        thing = self.s.get(
            self.base_url+'/thing/'+str(self.thing['id'])
        )
        if(thing.status_code == 200):
            update_thing(thing.json())
            self.thing = thing.json()
        if(r.status_code == 201 or 200):
            return r.json()   
        return None


    def get_unit(self, unit_name, abbreviation=None, unit_type=None):
        """Gets the unit with the specified name, or creates the unit but the abbreviation and the type must be provided """
        r = self.s.get(
            self.base_url+'/unit',
            params={'name': unit_name}
        )
        if r.status_code == 200 and len(r.json()):
            return r.json()[0]
        else:
            if not unit_type:
                print('No unit found')
                return
            data = {
                    'name': unit_name,
                    'type': unit_type
                }
            if abbreviation:
                data['abbreviation'] = abbreviation
            r = self.s.post(
                self.base_url+'/unit',
                data=data
            )
            if r.status_code < 400:
                return r.json()

    def get_sensors(self):
        if self.thing and 'sensors' in self.thing:
            return self.thing['sensors']
        return []

    def get_groups(self, user):
        pass
