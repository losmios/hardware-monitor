from config import load_config
import requests

def is_server_up():
    data = load_config()
    url = data['base_url']
    try:
        r = requests.get(url, timeout=10)
    except Exception:
        return False
    return r.status_code == 200